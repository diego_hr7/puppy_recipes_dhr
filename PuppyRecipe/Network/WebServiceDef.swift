//
//  WebServiceDef.swift
//  PuppyRecipe
//
//  Created by Armit Solutions on 3/31/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

enum WebServiceDef {
    case getRecipes(ingredients:[String]?, query:String?, page:Int)
    
    // Complete request URL
    var requestURL: String {
        switch self {
        case .getRecipes(let ingredients, let query, let page):
            var strParameters = Constants.Server.URL_SERVER + "?page=\(page)"
            if let strIngredients = ingredients?.joined(separator: ","){
                strParameters += "&i=\(strIngredients)"
                
            }
            if query != nil && !query!.isEmpty{
                strParameters += "&q=\(String(describing: query!))"
            }
            return strParameters
            
        }
    }
    var method: HTTPMethod {
        switch self {
        case .getRecipes:
            return .get
            
        }
    }
    var encoding: ParameterEncoding {
        switch self {
        case .getRecipes:
            return URLEncoding.default
        }
    }
    var parameters: [String:Any]? {
        return nil
    }
}


