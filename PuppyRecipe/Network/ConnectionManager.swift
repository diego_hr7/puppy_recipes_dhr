//
//  ConnectionManager.swift
//  PuppyRecipe
//
//  Created by Armit Solutions on 3/31/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper

class ConnectionManager{
    
    static func getRecipes(ingredients:[String]?, query:String?, page:Int, completion: @escaping (SearchRecipeResponse?)-> Void){
        let request: WebServiceDef = .getRecipes(ingredients: ingredients, query: query!, page: page)
        debugPrint("url:\(request.requestURL)")
        
        Alamofire.request(request.requestURL, method: request.method, parameters: request.parameters, encoding: request.encoding, headers: nil).validate().responseObject(completionHandler: { (response: DataResponse<SearchRecipeResponse>) in
            switch response.result{
            case .success:
                debugPrint("success")
                completion(response.value!)
                
            case .failure(let error):
                debugPrint("error - ", error)
                completion(nil)
            }
            
        })
    
    }
    
    
}

