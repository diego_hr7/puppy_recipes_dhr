//
//  SearchRecipeResponse.swift
//  PuppyRecipe
//
//  Created by Armit Solutions on 3/31/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation
import Foundation
import ObjectMapper

class SearchRecipeResponse:Mappable{
    var title:String!
    var ref:String!
    var recipes:[RecipeResponse]?
    
    required init?(map: Map) {
    }
    
    func mapping(map:Map){
        title <- map["title"]
        ref <- map["href"]
        recipes <- map["results"]
    }
}
