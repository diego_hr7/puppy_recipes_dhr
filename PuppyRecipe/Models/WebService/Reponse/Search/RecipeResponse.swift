//
//  RecipeResponse.swift
//  PuppyRecipe
//
//  Created by Armit Solutions on 3/31/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation
import ObjectMapper

class RecipeResponse:Mappable{
    var title:String!
    var ref:String!
    var ingredients:String?
    var image:String?
    
    required init?(map: Map) {
    }
    
    func mapping(map:Map){
        title <- map["title"]
        ref <- map["href"]
        ingredients <- map["ingredients"]
        image <- map["thumbnail"]
    }
}
