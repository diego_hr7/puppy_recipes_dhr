//
//  RecipeCell.swift
//  PuppyRecipe
//
//  Created by Armit Solutions on 3/31/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation
import UIKit

class RecipeCell:UITableViewCell{
    
    @IBOutlet weak var imgRecipe: UIImageView!
    @IBOutlet weak var lblTitleRecipe: UILabel!
    @IBOutlet weak var lblIngredients: UILabel!
    @IBOutlet weak var viewIngredientInd: UIView!
    
}
