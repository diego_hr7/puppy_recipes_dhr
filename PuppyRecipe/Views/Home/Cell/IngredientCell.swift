//
//  IngredientCell.swift
//  PuppyRecipe
//
//  Created by Armit Solutions on 3/31/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation
import UIKit

class IngredientCell:UITableViewCell{
    
    @IBOutlet weak var lblIngredient: UILabel!
    
    @IBOutlet weak var btnDelete: UIButton!
    
}
