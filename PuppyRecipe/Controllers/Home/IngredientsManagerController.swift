//
//  IngredientsManagerController.swift
//  PuppyRecipe
//
//  Created by Armit Solutions on 3/31/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation
import UIKit

protocol IngredientsDelegate {
    func didUpdateIngredients(ingredients:[String])
}

class IngredientsManagerController:BaseController{
    
    var ingredientsArray:[String]!
    var delegateIngredient:IngredientsDelegate?
    
    @IBOutlet weak var tblViewIngredient: UITableView!
    
    @IBOutlet weak var txtNewIngredient: UITextField!
    
    override func viewDidLoad() {
        self.tblViewIngredient.reloadData()
    }
    
    @IBAction func addIngredient(_ sender: Any) {
        let query = txtNewIngredient.text!
        if txtNewIngredient.text == ""{
            self.showSimpleAlert(title: nil, message: LocalizableStrings.Alerts.ALERT_EMPTY_ADD_INGREDIENT)
            return
        }else if ingredientsArray.contains(where: {$0.caseInsensitiveCompare(query) == .orderedSame}) {
            self.showSimpleAlert(title: nil, message: LocalizableStrings.Alerts.ALERT_REPEAT_ADD_INGREDIENT)
            return
        }
        ingredientsArray.append(txtNewIngredient.text!)
        txtNewIngredient.text = ""
        self.tblViewIngredient.reloadData()
    }
    @IBAction func accept(_ sender: Any) {
        self.delegateIngredient?.didUpdateIngredients(ingredients: ingredientsArray)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func deleteIngredient(_ sender:Any){
        let btn = sender as! UIButton
        ingredientsArray.remove(at: btn.tag)
        self.tblViewIngredient.reloadData()
    }
}

extension IngredientsManagerController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = ingredientsArray{
            return ingredientsArray!.count
        }
        return 0
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdenfiers.HOME_INGREDIENT_CELL) as! IngredientCell
        cell.lblIngredient.text = ingredientsArray[indexPath.row]
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(deleteIngredient), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
