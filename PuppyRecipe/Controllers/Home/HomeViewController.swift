//
//  ViewController.swift
//  PuppyRecipe
//
//  Created by Armit Solutions on 3/31/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import UIKit
import SVProgressHUD
import Kingfisher

class HomeViewController: BaseController,UIScrollViewDelegate {
    
    //OUTLETS
    @IBOutlet weak var tblViewRecipes: UITableView!
    @IBOutlet weak var searchRecipes: UISearchBar!
    @IBOutlet weak var bottomLoading: BottomLoading!
    @IBOutlet weak var btnIngredientsAdded: UIButton!
    
    private var recipesArray:[RecipeResponse]?
    let HEIGHT_BOTTOM_LOAD:CGFloat = 30
    
    private var ingredients:[String]? = nil
    private var currentPage:Int = 1
    private var query:String! = ""
    private var isLoadingMore = false
    private let refreshControl:UIRefreshControl = UIRefreshControl()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.refreshControl.addTarget(self, action:#selector(getRecipes) , for: .valueChanged)
        self.tblViewRecipes.refreshControl = self.refreshControl
        
        self.getRecipes()
    }
    override func viewDidLayoutSubviews() {
        self.tblViewRecipes.contentSize = CGSize(width: self.tblViewRecipes.contentSize.width, height: self.tblViewRecipes.contentSize.height + HEIGHT_BOTTOM_LOAD)
    }
    
    @objc func getRecipes(){
        self.refreshControl.endRefreshing()
        SVProgressHUD.show()
        if isLoadingMore == false{
            currentPage = 1
        }
        ConnectionManager.getRecipes(ingredients: ingredients, query: query, page: currentPage) { (response) in
            if response == nil{
                self.showSimpleAlert(title: "", message: LocalizableStrings.Alerts.ALERT_GENERIC_ERROR_MESSAGE)
            }else{
                if self.isLoadingMore{
                    self.recipesArray!.append(contentsOf: response!.recipes!)
                }else{
                    self.recipesArray = response!.recipes ?? []
                }
                
                DispatchQueue.main.async {
                    self.tblViewRecipes.reloadData()
                }
            }
            DispatchQueue.main.async {
                self.bottomLoading.isHidden = true
                self.view.isUserInteractionEnabled = true
                SVProgressHUD.dismiss()
            }
            self.isLoadingMore = false
        }
    }
    
    @IBAction func showIngredients(_ sender: Any) {
        
    }
    @IBAction func showFilters(_ sender: Any) {
        self.performSegue(withIdentifier: Constants.SegueIdentifiers.INGREDIENTS_MANAGE_SEGUE, sender: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.SegueIdentifiers.DETAIL_RECIPE_SEGUE{
            let detail = segue.destination as! DetailRecipeController
            detail.urlDetail = sender as! String
        }else if segue.identifier == Constants.SegueIdentifiers.INGREDIENTS_MANAGE_SEGUE{
            let ing = segue.destination as! IngredientsManagerController
            ing.ingredientsArray = ingredients ?? []
            ing.delegateIngredient = self
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height && isLoadingMore == false{
            isLoadingMore = true
            currentPage += 1
            self.bottomLoading.isHidden = false
            self.view.isUserInteractionEnabled = false
            self.getRecipes()
        }
    }
}

extension HomeViewController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = recipesArray{
            return recipesArray!.count
        }
        return 0
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdenfiers.HOME_RECIPE_CELL) as! RecipeCell
        let recipe:RecipeResponse = self.recipesArray![indexPath.row]
        if let _ = recipe.image{
            if let url = URL(string: recipe.image!){
                cell.imgRecipe.kf.setImage(with: url)
            }
        }
        cell.lblTitleRecipe.text = recipe.title
        cell.lblIngredients.text = LocalizableStrings.Home.LBL_INGREDIENTS_TITLE + (recipe.ingredients ?? "")
        cell.viewIngredientInd.backgroundColor = UIColor(hexString: IngredientIndicator.defaultValue.rawValue)
        for i in IngredientIndicator.allValues{
            //debugPrint(i.name)
            if recipe.ingredients!.contains(i.name){
                cell.viewIngredientInd.backgroundColor =  UIColor(hexString: i.rawValue)
                break
            }
        }

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let recipe:RecipeResponse = self.recipesArray![indexPath.row]
        tableView.deselectRow(at: indexPath, animated: false)
        self.performSegue(withIdentifier: Constants.SegueIdentifiers.DETAIL_RECIPE_SEGUE, sender: recipe.ref)
    }
}

extension HomeViewController:UISearchBarDelegate{
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchRecipes.text = ""
        //refresh
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.query = searchBar.text!
        if query.isEmpty{
            self.showSimpleAlert(title: nil, message: LocalizableStrings.Alerts.ALERT_EMPTY_SEARCH)
            return
        }
        self.getRecipes()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //self.view.endEditing(true)
    }
    
}

extension HomeViewController:IngredientsDelegate{
    func didUpdateIngredients(ingredients: [String]) {
        self.ingredients = ingredients
        if ingredients.count>0{
            btnIngredientsAdded.setTitle("\(ingredients.count) agregados", for: .normal)
        }else{
            btnIngredientsAdded.setTitle(LocalizableStrings.Home.LBL_EMPTY_INGREDIENTS, for: .normal)
        }
        self.getRecipes()
    }
}


