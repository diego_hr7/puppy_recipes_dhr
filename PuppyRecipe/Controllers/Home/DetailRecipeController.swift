//
//  DetailRecipeController.swift
//  PuppyRecipe
//
//  Created by Armit Solutions on 3/31/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation
import WebKit
import SVProgressHUD

class DetailRecipeController:BaseController, UIWebViewDelegate{
    
    var urlDetail:String!
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        if let url = URL(string: urlDetail){
            
            let request = URLRequest(url: url)
            self.webView.loadRequest(request)
        }
    }
    @IBAction func back(_ sender: Any) {
       SVProgressHUD.dismiss()
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
            SVProgressHUD.show()

    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SVProgressHUD.dismiss()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        //self.showSimpleAlert(title: nil, message: LocalizableStrings.Alerts.ALERT_GENERIC_ERROR_MESSAGE)
    }
    
}
