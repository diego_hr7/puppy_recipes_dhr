//
//  LocalizableStrings.swift
//  PuppyRecipe
//
//  Created by Armit Solutions on 3/31/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation

struct LocalizableStrings{
    
    struct Alerts{
        static let ALERT_GENERIC_ERROR_MESSAGE = NSLocalizedString("alert.generic.error.message", comment: "")
        static let ALERT_GENERIC_ERROR_BUTTON = NSLocalizedString("alert.generic.error.button", comment: "")
        static let ALERT_EMPTY_SEARCH = NSLocalizedString("alert.empty.search", comment: "")
        static let ALERT_EMPTY_ADD_INGREDIENT = NSLocalizedString("alert.empty.add.ingredient", comment: "")
        static let ALERT_REPEAT_ADD_INGREDIENT = NSLocalizedString("alert.repeat.add.ingredient", comment: "")
        
    }
    
    struct Home{
        static let LBL_TITLE_HOME = NSLocalizedString("lbl.title.home", comment: "")
        static let LBL_INGREDIENTS_TITLE = NSLocalizedString("lbl.ingredients.title", comment: "")
         static let LBL_EMPTY_INGREDIENTS = NSLocalizedString("lbl.empty.ingredients", comment: "")
    }
}
