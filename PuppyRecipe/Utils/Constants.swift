//
//  Constatns.swift
//  PuppyRecipe
//
//  Created by Armit Solutions on 3/31/19.
//  Copyright © 2019 Armit Solutions. All rights reserved.
//

import Foundation

struct Constants{
    struct Server{
        static let URL_SERVER = "http://www.recipepuppy.com/api/"
    }
    
    struct CellIdenfiers{
        static let HOME_RECIPE_CELL = "RecipeCell"
        static let HOME_INGREDIENT_CELL = "IngredientCell"
    }
    struct SegueIdentifiers{
        static let DETAIL_RECIPE_SEGUE = "recipeDetail"
        static let INGREDIENTS_MANAGE_SEGUE = "segueIngredients"
    }
}

enum IngredientIndicator:String { // se pone los ingredientes mas comunes y se les asigna un color aleatorio
    case cheddar = "#FFFF00"
    case egg = "#FFFACD"
    case pork = "#FFDAB9"
    case chicken = "#f49e42"
    case garlic = "#2b4f9b"
    case sugar = "#bd45d8"
    case milk = "#39cebd"
    case orange = "#ce9239"
    case defaultValue = "#BBBBBB"
    
    static let allValues = [cheddar, egg, pork, chicken, garlic, sugar, milk,orange,defaultValue]
    var name: String {
        get { return String(describing: self) }
    }
}
extension IngredientIndicator: CaseIterable {}
